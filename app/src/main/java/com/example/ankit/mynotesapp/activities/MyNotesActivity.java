package com.example.ankit.mynotesapp.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.ShareCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ankit.mynotesapp.R;
import com.example.ankit.mynotesapp.adapters.NotesAdapter;
import com.example.ankit.mynotesapp.filters.NotesFilter;
import com.example.ankit.mynotesapp.gestures.ItemTouchHelperAdapter;
import com.example.ankit.mynotesapp.gestures.SimpleItemTouchHelperCallback;
import com.example.ankit.mynotesapp.interfaces.TaskFinished;
import com.example.ankit.mynotesapp.models.Note;
import com.example.ankit.mynotesapp.requestDataModels.DatabaseRequestHandler;
import com.example.ankit.mynotesapp.requestDataModels.PreferencesRequestHandler;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class MyNotesActivity extends AppCompatActivity implements NotesFilter.FilterResultListener {

    public static final int REQUEST_CREATE_NOTE = 1000;

    private FloatingActionButton mFab;
    private RecyclerView mRvNotes;
    private TextView mTvEmptyView;
    private SearchView mSearchView;

    private NotesAdapter mNotesAdapter;
    private List<Note> mNotes;
    private List<Note> mFilteredNotes;

    private NotesFilter mNotesFilter;

    private ProgressDialog mPDHome;

    //callback for fetch notes
    private TaskFinished<List<Note>> mFetchNotesCallback;
    private TaskFinished<Boolean> mUpdateNotesLockedStatusCallback;
    private TaskFinished<Boolean> mUpdateTitleCallback;
    private TaskFinished<Boolean> mDeleteNoteCallback;
    private PreferencesRequestHandler.PasswordSetCallback mPasswordSetCallback;
    private PreferencesRequestHandler.PasswordValidationCallback mPasswordValidationCallback;

    //variables to keep track of note position when context menu opens
    private int mPosition = -1;

    //notes sorting order
    private int mSortBy = DatabaseRequestHandler.SORT_BY_LAST_EDITED_DATE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_notes);

        //set toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //set fab
        mFab = (FloatingActionButton) findViewById(R.id.fab);
        if (mFab != null) {
            mFab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    hideFab();
                    startCreateNoteActivity();
                }
            });
        }

        //set recycler view and its adapter
        mRvNotes = (RecyclerView) findViewById(R.id.rv_notes);
        if (mRvNotes != null) {
            mRvNotes.setLayoutManager(new LinearLayoutManager(this));
            mNotes = new ArrayList<>();
            mFilteredNotes = new ArrayList<>();
            mNotesAdapter = new NotesAdapter(this, mFilteredNotes, mSortBy);
            mRvNotes.setAdapter(mNotesAdapter);

            mNotesFilter = new NotesFilter(this, mNotes);

            //set swipe dismiss behavior
            ItemTouchHelper.Callback callback =
                    new SimpleItemTouchHelperCallback(new ItemTouchHelperAdapter() {
                        @Override
                        public boolean onItemMove(int fromPosition, int toPosition) {
                            return false;
                        }

                        @Override
                        public void onItemDismiss(final int position) {

                            final Note note = mFilteredNotes.remove(position);
                            mNotes.remove(note);
                            mNotesAdapter.notifyItemRemoved(position);

                            Snackbar.make(mRvNotes, "Note deleted", Snackbar.LENGTH_SHORT)
                                    .setAction(R.string.action_undo, new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            mFilteredNotes.add(position, note);
                                            mNotes.add(note);
                                            mNotesAdapter.notifyItemInserted(position);
                                        }
                                    }).show();
                        }
                    }) {
                        @Override
                        public float getSwipeThreshold(RecyclerView.ViewHolder viewHolder) {
                            return 0.75f;
                        }
                    };
            ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
            touchHelper.attachToRecyclerView(mRvNotes);
        }

        //set empty view
        mTvEmptyView = (TextView) findViewById(R.id.tv_empty_view);

        //set progress dialog
        mPDHome = new ProgressDialog(this);
        mPDHome.setCancelable(false);
        mPDHome.setMessage("Please wait...");
    }

    @Override
    protected void onResume() {
        super.onResume();
        fetchNotes();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_my_notes, menu);

        mSearchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        mSearchView.setIconifiedByDefault(true);
        mSearchView.setQueryHint(getString(R.string.search_hint));

        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mNotesFilter.filter(newText);
                return false;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.action_sort:
                if (mSortBy == DatabaseRequestHandler.SORT_BY_LAST_EDITED_DATE) {
                    mSortBy = DatabaseRequestHandler.SORT_BY_CREATED_DATE;
                    item.setTitle(R.string.menu_sort_by_edit_time);
                } else {
                    mSortBy = DatabaseRequestHandler.SORT_BY_LAST_EDITED_DATE;
                    item.setTitle(R.string.menu_sort_by_create_time);
                }
                mNotesAdapter.setSortBy(mSortBy);
                fetchNotes();
                break;

            case R.id.action_change_password:
                changePassword();
                break;

            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQUEST_CREATE_NOTE:
                showFab();
                break;

            default:
                break;
        }
    }

    @Override
    public void finish() {

        //dismiss dialog if not null and is shown and make it null
        if (mPDHome != null) {
            mPDHome.dismiss();
            mPDHome = null;
        }

        //unset callbacks if set
        mFetchNotesCallback = null;
        mUpdateNotesLockedStatusCallback = null;
        mUpdateTitleCallback = null;
        mDeleteNoteCallback = null;
        mPasswordSetCallback = null;
        mPasswordValidationCallback = null;

        super.finish();
    }

    private void hideFab() {
        // get the center for the clipping circle
        int cx = mFab.getWidth() / 2;
        int cy = mFab.getHeight() / 2;

        // get the initial radius for the clipping circle
        float initialRadius = (float) Math.hypot(cx, cy);

        // create the animator for this view (the final radius is zero)
        Animator anim = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            anim = ViewAnimationUtils.createCircularReveal(mFab, cx, cy, initialRadius, 0);
        } else {
            mFab.setVisibility(View.INVISIBLE);
        }

        if (anim != null) {
            // make the view invisible when the animation is done
            anim.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    mFab.setVisibility(View.INVISIBLE);
                }
            });
            anim.start();
        }
    }

    private void showFab() {
        // get the center for the clipping circle
        int cx = mFab.getWidth() / 2;
        int cy = mFab.getHeight() / 2;

        // get the final radius for the clipping circle
        float finalRadius = (float) Math.hypot(cx, cy);

        // create the animator for this view (the start radius is zero)
        Animator anim = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            anim = ViewAnimationUtils.createCircularReveal(mFab, cx, cy, 0, finalRadius);
        }

        // make the view visible and start the animation
        mFab.setVisibility(View.VISIBLE);
        if (anim != null) {
            anim.start();
        }
    }

    private void onLoadComplete(String errorMessage) {

        if (mPDHome != null) {
            mPDHome.dismiss();
        }

        if (!TextUtils.isEmpty(errorMessage)) {
            Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
        }
    }

    private void fetchNotes() {

        //show progress dialog
        mPDHome.show();

        //initialize callback
        mFetchNotesCallback = new TaskFinished<List<Note>>() {
            @Override
            public void onTaskFinished(List<Note> notes, String errorMessage) {

                if (TextUtils.isEmpty(errorMessage)) {
                    //unset the callback
                    mFetchNotesCallback = null;

                    if (notes == null || notes.size() == 0) {
                        mTvEmptyView.setVisibility(View.VISIBLE);
                        mRvNotes.setVisibility(View.GONE);
                    } else {
                        mTvEmptyView.setVisibility(View.GONE);
                        mRvNotes.setVisibility(View.VISIBLE);
                        //set data in list and notify adapter's data set changed
                        mNotes.clear();
                        mFilteredNotes.clear();
                        mNotes.addAll(notes);
                        mFilteredNotes.addAll(notes);
                        mNotesAdapter.notifyDataSetChanged();
                    }
                }

                onLoadComplete(errorMessage);
            }
        };

        //fetch notes from DB
        DatabaseRequestHandler.fetchNotes(mFetchNotesCallback, mSortBy);
    }

    @Override
    public void onFilter(List<Note> notes) {
        mFilteredNotes.clear();
        mFilteredNotes.addAll(notes);
        mNotesAdapter.notifyDataSetChanged();

        if (mFilteredNotes.size() == 0) {
            mRvNotes.setVisibility(View.GONE);
            mTvEmptyView.setVisibility(View.VISIBLE);
        } else {
            mRvNotes.setVisibility(View.VISIBLE);
            mTvEmptyView.setVisibility(View.GONE);
        }
    }

    private void changePassword() {
        //initialize callback
        mPasswordSetCallback = new PreferencesRequestHandler.PasswordSetCallback() {
            @Override
            public void onPasswordSet(boolean success, int messageId) {
                //unset the callback
                mPasswordSetCallback = null;

                //show message
                Toast.makeText(MyNotesActivity.this, messageId, Toast.LENGTH_SHORT).show();
            }
        };

        if (PreferencesRequestHandler.isPasswordSet()) {
            //prompt user to save password
            PreferencesRequestHandler.promptUserToResetPassword(new WeakReference<Context>(this),
                    new WeakReference<>(mPasswordSetCallback));
        } else {
            //prompt user to save password
            PreferencesRequestHandler.promptUserToSetPassword(new WeakReference<Context>(this),
                    new WeakReference<>(mPasswordSetCallback));
        }
    }

    private void startCreateNoteActivity() {

        //reset search field if it contains some value
        if (mSearchView != null) {
            mSearchView.setQuery("", false);
            mSearchView.setIconified(true);
        }

        //start activity with scene transition
        startActivityForResult(new Intent(this, CreateNoteActivity.class),
                REQUEST_CREATE_NOTE,
                ActivityOptionsCompat.makeSceneTransitionAnimation(this).toBundle());
    }

    public void startEditNoteActivity(final int position, final View view) {

        //reset search field if it contains some value
        if (mSearchView != null) {
            mSearchView.setQuery("", false);
            mSearchView.setIconified(true);
        }

        final Note note = mFilteredNotes.get(position);

        if (note.isLocked) {
            //initialize callback
            mPasswordValidationCallback = new PreferencesRequestHandler.PasswordValidationCallback() {
                @Override
                public void onPasswordValidation(boolean success, int messageId) {
                    //unset the callback
                    mPasswordValidationCallback = null;

                    //if success then change lock status
                    if (success) {
                        Intent intent = new Intent(MyNotesActivity.this, EditNoteActivity.class);
                        intent.putExtra(EditNoteActivity.INTENT_EXTRA_NOTE, note);

                        //start activity with scene transition
                        startActivity(intent, ActivityOptionsCompat
                                .makeSceneTransitionAnimation(MyNotesActivity.this, view, getString(R.string.transition_description)).toBundle());
                    }

                    //show message
                    if (messageId != PreferencesRequestHandler.NO_MESSAGE_ID) {
                        Toast.makeText(MyNotesActivity.this, messageId, Toast.LENGTH_SHORT).show();
                    }
                }
            };

            PreferencesRequestHandler.promptUserToEnterPasswordAndValidate(new WeakReference<Context>(this),
                    new WeakReference<>(mPasswordValidationCallback));
        } else {
            Intent intent = new Intent(this, EditNoteActivity.class);
            intent.putExtra(EditNoteActivity.INTENT_EXTRA_NOTE, note);

            //start activity with scene transition
            startActivity(intent, ActivityOptionsCompat
                    .makeSceneTransitionAnimation(this, view, getString(R.string.transition_description)).toBundle());
        }
    }

    public void onCreateContextMenu(ContextMenu menu, int position) {
        Note note = mFilteredNotes.get(position);
        mPosition = position;

        //set menu header title
        menu.setHeaderTitle(note.title);

        //inflate menu
        getMenuInflater().inflate(R.menu.menu_note_card, menu);

        //set title for action lock based on locked state of the note
        menu.findItem(R.id.action_lock).setTitle(note.isLocked ? R.string.menu_unlock : R.string.menu_lock);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_change_title:
                changeNoteTitle(mPosition);
                mPosition = -1;
                break;

            case R.id.action_lock:
                changeLockedStatus(mPosition);
                mPosition = -1;
                break;

            case R.id.action_share:
                shareNote(mPosition);
                break;

            case R.id.action_delete:
                showDeleteNoteAlert(mPosition);
                mPosition = -1;
                break;

            default:
                break;
        }

        return super.onContextItemSelected(item);
    }

    private void changeNoteTitle(final int position) {

        final Note note = mFilteredNotes.get(position);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.enter_title);

        // Set up the input
        final EditText etNoteTitle = new EditText(this);
        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        etNoteTitle.setInputType(InputType.TYPE_CLASS_TEXT);

        //set hint text
        etNoteTitle.setHint(R.string.enter_title);

        //set previous text
        etNoteTitle.setText(note.title);

        //set maxLines 1
        etNoteTitle.setMaxLines(1);

        //set max length for edit text (50 characters)
        etNoteTitle.setFilters(new InputFilter[]{new InputFilter.LengthFilter(50)});


        //set view for alert dialog
        builder.setView(etNoteTitle);

        // Set up the buttons
        builder.setPositiveButton("OK", null);
        builder.setNegativeButton("Cancel", null);

        final AlertDialog alertDialog = builder.create();

        //set up button listeners
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Button btnOk = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);

                //set listener on ok button
                btnOk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String title = etNoteTitle.getText().toString().trim();

                        if (TextUtils.isEmpty(title)) {
                            //set title field empty
                            etNoteTitle.setText("");

                            //set "Field Required" error
                            etNoteTitle.setError(MyNotesActivity.this.getString(R.string.error_msg_field_required));

                            etNoteTitle.requestFocus();
                        } else {
                            changeNoteTitleInDB(note, position, title);
                        }

                        alertDialog.dismiss();
                    }
                });
            }
        });

        alertDialog.show();
    }

    private void changeNoteTitleInDB(final Note note, final int position, final String title) {

        //show progress dialog
        if (mPDHome != null) {
            mPDHome.show();
        }

        mUpdateTitleCallback = new TaskFinished<Boolean>() {
            @Override
            public void onTaskFinished(Boolean success, String errorMessage) {
                //unset the callback
                mUpdateTitleCallback = null;

                //dismiss the dialog if not null (checking null to prevent not attached to window error)
                if (mPDHome != null) {
                    mPDHome.dismiss();
                }

                //if error message is not null then show error message
                if (TextUtils.isEmpty(errorMessage)) {
                    if (success != null && success) {
                        //remove note from the list and notify data set changed
                        note.title = title;
                        mNotesAdapter.notifyItemChanged(position);

                        //if success then show toast and finish
                        Toast.makeText(MyNotesActivity.this, R.string.update_success, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(MyNotesActivity.this, R.string.error_msg_common, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(MyNotesActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
                }
            }
        };

        DatabaseRequestHandler.updateNotesTitle(note.noteId, note.title, mUpdateTitleCallback);
    }

    private void showDeleteNoteAlert(final int position) {

        final Note note = mFilteredNotes.get(position);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.delete_note);

        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                deleteNote(note, position);
                dialog.dismiss();
            }
        });

        builder.setNegativeButton("Cancel", null);

        builder.setCancelable(false).show();
    }

    private void deleteNote(final Note note, final int position) {

        //show progress dialog
        if (mPDHome != null) {
            mPDHome.show();
        }

        mDeleteNoteCallback = new TaskFinished<Boolean>() {
            @Override
            public void onTaskFinished(Boolean success, String errorMessage) {
                //unset the callback
                mDeleteNoteCallback = null;

                //dismiss the dialog if not null (checking null to prevent not attached to window error)
                if (mPDHome != null) {
                    mPDHome.dismiss();
                }

                //if error message is not null then show error message
                if (TextUtils.isEmpty(errorMessage)) {
                    if (success != null && success) {
                        //remove note from the list and notify data set changed
                        mFilteredNotes.remove(position);
                        mNotes.remove(note);
                        mNotesAdapter.notifyItemRemoved(position);

                        if (mFilteredNotes.size() == 0) {
                            mRvNotes.setVisibility(View.GONE);
                            mTvEmptyView.setVisibility(View.VISIBLE);
                        } else {
                            mRvNotes.setVisibility(View.VISIBLE);
                            mTvEmptyView.setVisibility(View.GONE);
                        }

                        //if success then show toast and finish
                        Toast.makeText(MyNotesActivity.this, R.string.delete_success, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(MyNotesActivity.this, R.string.error_msg_common, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(MyNotesActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
                }
            }
        };

        DatabaseRequestHandler.updateNotesDeleteStatus(note.noteId, !note.isDeleted, mDeleteNoteCallback);
    }

    public void changeLockedStatus(int position) {
        Note note = mFilteredNotes.get(position);

        if (note.isLocked) {
            unlockNote(note, position);
        } else {
            lockNote(note, position);
        }
    }

    private void unlockNote(final Note note, final int position) {
        //initialize callback
        mPasswordValidationCallback = new PreferencesRequestHandler.PasswordValidationCallback() {
            @Override
            public void onPasswordValidation(boolean success, int messageId) {
                //unset the callback
                mPasswordValidationCallback = null;

                //if success then change lock status
                if (success) {
                    updateLockedStatusInDB(note, position);
                }

                //show message
                if (messageId != PreferencesRequestHandler.NO_MESSAGE_ID) {
                    Toast.makeText(MyNotesActivity.this, messageId, Toast.LENGTH_SHORT).show();
                }
            }
        };

        PreferencesRequestHandler.promptUserToEnterPasswordAndValidate(new WeakReference<Context>(this),
                new WeakReference<>(mPasswordValidationCallback));
    }

    private void lockNote(final Note note, final int position) {
        //if shared preferences contains password then lock otherwise prompt user to set password
        if (PreferencesRequestHandler.isPasswordSet()) {
            updateLockedStatusInDB(note, position);
        } else {
            //initialize callback
            mPasswordSetCallback = new PreferencesRequestHandler.PasswordSetCallback() {
                @Override
                public void onPasswordSet(boolean success, int messageId) {
                    //unset the callback
                    mPasswordSetCallback = null;

                    if (success) {
                        //update locked status in db
                        updateLockedStatusInDB(note, position);
                    }

                    Toast.makeText(MyNotesActivity.this, messageId, Toast.LENGTH_SHORT).show();
                }
            };

            //prompt user to save password
            PreferencesRequestHandler.promptUserToSetPassword(new WeakReference<Context>(this),
                    new WeakReference<>(mPasswordSetCallback));
        }
    }

    //this function updates notes locked status in db
    private void updateLockedStatusInDB(final Note note, final int position) {

        //show progress dialog
        if (mPDHome != null) {
            mPDHome.show();
        }

        mUpdateNotesLockedStatusCallback = new TaskFinished<Boolean>() {
            @Override
            public void onTaskFinished(Boolean success, String errorMessage) {

                //unset the callback
                mUpdateNotesLockedStatusCallback = null;

                //dismiss the dialog if not null (checking null to prevent not attached to window error)
                if (mPDHome != null) {
                    mPDHome.dismiss();
                }

                //if error message is not empty then show error message
                if (TextUtils.isEmpty(errorMessage)) {
                    if (success != null && success) {

                        //update note's lock status in local variable
                        note.isLocked = !note.isLocked;

                        //update note's data and notify adapter data changed
                        mNotesAdapter.notifyItemChanged(position);
                    } else {
                        Toast.makeText(MyNotesActivity.this, R.string.error_msg_common, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(MyNotesActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
                }
            }
        };

        DatabaseRequestHandler.updateNotesLockedStatus(note.noteId, !note.isLocked, mUpdateNotesLockedStatusCallback);
    }

    private void shareNote(int position) {
        String shareText = mFilteredNotes.get(position).title + "\n" + mFilteredNotes.get(position).description;
        ShareCompat.IntentBuilder
                .from(this)
                .setChooserTitle(R.string.share_note)
                .setType("text/plain")
                .setText(shareText)
                .startChooser();
    }
}
