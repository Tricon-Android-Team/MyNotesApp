package com.example.ankit.mynotesapp.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Calendar;

public class Reminder implements Parcelable {

    //reminder id
    public int reminderId;

    public int year, month, day, hour, minute;

    public long time;

    public Reminder() {
        final Calendar calendar = Calendar.getInstance();
        this.year = calendar.get(Calendar.YEAR);
        this.month = calendar.get(Calendar.MONTH);
        this.day = calendar.get(Calendar.DAY_OF_MONTH);
        this.hour = calendar.get(Calendar.HOUR_OF_DAY);
        this.minute = calendar.get(Calendar.MINUTE);
        this.time = calendar.getTimeInMillis();
    }

    public Reminder(int reminderId, long time) {
        this.reminderId = reminderId;

        final Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        this.year = calendar.get(Calendar.YEAR);
        this.month = calendar.get(Calendar.MONTH);
        this.day = calendar.get(Calendar.DAY_OF_MONTH);
        this.hour = calendar.get(Calendar.HOUR_OF_DAY);
        this.minute = calendar.get(Calendar.MINUTE);
        this.time = calendar.getTimeInMillis();
    }

    protected Reminder(Parcel in) {
        reminderId = in.readInt();
        year = in.readInt();
        month = in.readInt();
        day = in.readInt();
        hour = in.readInt();
        minute = in.readInt();
        time = in.readLong();
    }

    public void resetTime() {
        final Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day, hour, minute);
        time = calendar.getTimeInMillis();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(reminderId);
        dest.writeInt(year);
        dest.writeInt(month);
        dest.writeInt(day);
        dest.writeInt(hour);
        dest.writeInt(minute);
        dest.writeLong(time);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Reminder> CREATOR = new Creator<Reminder>() {
        @Override
        public Reminder createFromParcel(Parcel in) {
            return new Reminder(in);
        }

        @Override
        public Reminder[] newArray(int size) {
            return new Reminder[size];
        }
    };
}
