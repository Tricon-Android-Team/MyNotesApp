package com.example.ankit.mynotesapp.activities;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.ankit.mynotesapp.R;
import com.example.ankit.mynotesapp.interfaces.TaskFinished;
import com.example.ankit.mynotesapp.models.Reminder;
import com.example.ankit.mynotesapp.requestDataModels.DatabaseRequestHandler;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class SetReminderActivity extends AppCompatActivity implements
        View.OnClickListener {

    private TextView mTvDate;
    private TextView mTvTime;

    private Reminder mReminder;

    private ProgressDialog mPDSetReminder;
    private TaskFinished<Long> mSetReminderCallback;

    public static final String INTENT_EXTRA_REMINDER_ID = "REMINDER_ID";
    public static final int REQUEST_BOOT_COMPLETE_PERMISSION = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_reminder);

        //set toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (toolbar != null) {
            toolbar.setNavigationIcon(R.drawable.ic_close_white);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

        //set up view
        Button btnDatePicker = (Button) findViewById(R.id.btn_date);
        Button btnTimePicker = (Button) findViewById(R.id.btn_time);
        mTvDate = (TextView) findViewById(R.id.tv_date);
        mTvTime = (TextView) findViewById(R.id.tv_time);

        //set click listeners
        if (btnDatePicker != null) {
            btnDatePicker.setOnClickListener(this);
        }

        if (btnTimePicker != null) {
            btnTimePicker.setOnClickListener(this);
        }

        //set reminder
        mReminder = new Reminder();

        //set progress dialog
        mPDSetReminder = new ProgressDialog(this);
        mPDSetReminder.setCancelable(false);
        mPDSetReminder.setMessage("Please wait...");

        //set default result as canceled
        setResult(RESULT_CANCELED);

        //get boot complete permission
        getBootCompletePermission();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_set_reminder, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_done:
                validateAndSetReminder();
                break;

            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btn_date:
                showDatePickerDialog();
                break;

            case R.id.btn_time:
                showTimePickerDialog();
                break;

            default:
                break;
        }
    }

    @Override
    public void finish() {

        //dismiss dialog if not null and is shown and make it null
        if (mPDSetReminder != null) {
            mPDSetReminder.dismiss();
            mPDSetReminder = null;
        }

        //unset callbacks if set
        mSetReminderCallback = null;

        super.finish();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case REQUEST_BOOT_COMPLETE_PERMISSION:

                if (permissions[0].equals(Manifest.permission.RECEIVE_BOOT_COMPLETED)
                        && grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    Snackbar.make(mTvDate, R.string.msg_boot_complete_permission_required, Snackbar.LENGTH_SHORT).show();
                }
                break;

            default:
                break;
        }
    }

    public void getBootCompletePermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.RECEIVE_BOOT_COMPLETED) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.RECEIVE_BOOT_COMPLETED)) {
                Snackbar.make(mTvDate, R.string.msg_boot_complete_permission_required, Snackbar.LENGTH_INDEFINITE)
                        .setAction(R.string.allow, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                ActivityCompat.requestPermissions(SetReminderActivity.this,
                                        new String[]{Manifest.permission.RECEIVE_BOOT_COMPLETED},
                                        REQUEST_BOOT_COMPLETE_PERMISSION);
                            }
                        })
                        .show();
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.RECEIVE_BOOT_COMPLETED},
                        REQUEST_BOOT_COMPLETE_PERMISSION);
            }

        }
    }

    private void showDatePickerDialog() {
        // Get Current Date
        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date(mReminder.time));
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        //set min date today
        long minDate = calendar.getTimeInMillis();

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        mTvDate.setText(String.format(Locale.getDefault(), "%d-%d-%d", dayOfMonth, monthOfYear + 1, year));

                        mReminder.year = year;
                        mReminder.month = monthOfYear;
                        mReminder.day = dayOfMonth;
                        mReminder.resetTime();
                    }
                }, year, month, day);

        datePickerDialog.getDatePicker().setMinDate(minDate);

        datePickerDialog.show();
    }

    private void showTimePickerDialog() {
        // Get Current Time
        final Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        mTvTime.setText(String.format(Locale.getDefault(), "%d:%d", hourOfDay, minute));

                        mReminder.hour = hourOfDay;
                        mReminder.minute = minute;
                        mReminder.resetTime();
                    }
                }, hour, minute, false);

        timePickerDialog.show();
    }

    private void validateAndSetReminder() {
        if (mReminder.time < SystemClock.currentThreadTimeMillis()) {
            Toast.makeText(this, R.string.error_msg_incorrect_time, Toast.LENGTH_SHORT).show();
        } else {
            mSetReminderCallback = new TaskFinished<Long>() {
                @Override
                public void onTaskFinished(Long reminderId, String errorMessage) {

                    //unset the callback
                    mSetReminderCallback = null;

                    //dismiss the dialog if not null (checking null to prevent not attached to window error)
                    if (mPDSetReminder != null) {
                        mPDSetReminder.dismiss();
                    }

                    if (TextUtils.isEmpty(errorMessage)) {
                        if (reminderId != null && reminderId != -1) {
                            Intent resultIntent = new Intent();
                            resultIntent.putExtra(INTENT_EXTRA_REMINDER_ID, reminderId);

                            setResult(RESULT_OK, resultIntent);
                            finish();
                        } else {
                            Toast.makeText(SetReminderActivity.this, R.string.error_msg_common, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(SetReminderActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
                    }
                }
            };

            DatabaseRequestHandler.insertReminder(mReminder, mSetReminderCallback);
        }
    }
}
