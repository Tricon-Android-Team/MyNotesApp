package com.example.ankit.mynotesapp.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ShareCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.transition.Explode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.ankit.mynotesapp.R;
import com.example.ankit.mynotesapp.common.Utils;
import com.example.ankit.mynotesapp.customs.LinedEditText;
import com.example.ankit.mynotesapp.interfaces.TaskFinished;
import com.example.ankit.mynotesapp.models.Note;
import com.example.ankit.mynotesapp.requestDataModels.DatabaseRequestHandler;
import com.example.ankit.mynotesapp.requestDataModels.PreferencesRequestHandler;

import java.lang.ref.WeakReference;

public class EditNoteActivity extends AppCompatActivity {

    private ProgressDialog mPDEditNotes;
    private LinedEditText mETDescription;

    private Note mNote;
    private MenuItem mLockMenuItem;

    private TaskFinished<Boolean> mUpdateNoteCallback;
    private TaskFinished<Boolean> mDeleteNoteCallback;
    private PreferencesRequestHandler.PasswordSetCallback mPasswordSetCallback;

    public static final String INTENT_EXTRA_NOTE = "INTENT_EXTRA_NOTE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_note);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // set an exit transition
            getWindow().setExitTransition(new Explode());

            // set an enter transition
            getWindow().setEnterTransition(new Explode());
        }

        //set toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (toolbar != null) {
            toolbar.setNavigationIcon(R.drawable.ic_close_white);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

        //get intent extra
        mNote = getIntent().getExtras().getParcelable(INTENT_EXTRA_NOTE);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(mNote.title);
            actionBar.setSubtitle(Utils.getDate(mNote.lastEditedTime));
        }

        //set views
        mETDescription = (LinedEditText) findViewById(R.id.et_description);
        if (mETDescription != null) {
            mETDescription.setText(mNote.description);
        }

        //set progress dialog
        mPDEditNotes = new ProgressDialog(this);
        mPDEditNotes.setCancelable(false);
        mPDEditNotes.setMessage("Please wait...");
    }

    @Override
    public void finish() {

        //dismiss dialog if not null and is shown and make it null
        if (mPDEditNotes != null) {
            mPDEditNotes.dismiss();
            mPDEditNotes = null;
        }

        //unset callbacks if set
        mUpdateNoteCallback = null;
        mDeleteNoteCallback = null;
        mPasswordSetCallback = null;

        super.finish();
    }

    @Override
    public void onBackPressed() {
        //validate changes and save note
        validateChangesAndSave();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edit_note, menu);

        mLockMenuItem = menu.findItem(R.id.action_lock);

        if (mNote.isLocked) {
            mLockMenuItem.setTitle(R.string.menu_unlock);
        } else {
            mLockMenuItem.setTitle(R.string.menu_lock);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_save:
                //validate changes and save note
                validateChangesAndSave();
                break;

            case R.id.action_change_title:
                changeNoteTitle();
                break;

            case R.id.action_lock:
                lockUnlockNote();
                break;

            case R.id.action_share:
                shareNote();
                break;

            case R.id.action_cancel:
                finish();
                break;

            case R.id.action_delete:
                //show alert before delete and if user says yes then delete
                showDeleteNoteAlert();
                break;

            case R.id.action_change_background:
                break;

            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    //If user has typed some note and pressed back then don't ask title, auto set title as first word of the note and save
    //If user pressed save menu option then ask for title and save
    private void validateChangesAndSave() {

        if (mETDescription != null) {

            String description = mETDescription.getText().toString().trim();

            if (TextUtils.isEmpty(description)) {
                Toast.makeText(this, R.string.error_msg_empty_note, Toast.LENGTH_SHORT).show();
            } else {
                //show progress dialog
                if (mPDEditNotes != null) {
                    mPDEditNotes.show();
                }

                mNote.lastEditedTime = System.currentTimeMillis();
                mNote.description = description;

                mUpdateNoteCallback = new TaskFinished<Boolean>() {
                    @Override
                    public void onTaskFinished(Boolean success, String errorMessage) {
                        //unset the callback
                        mUpdateNoteCallback = null;

                        //dismiss the dialog if not null (checking null to prevent not attached to window error)
                        if (mPDEditNotes != null) {
                            mPDEditNotes.dismiss();
                        }

                        //if error message is not null then show error message
                        if (TextUtils.isEmpty(errorMessage)) {
                            if (success != null && success) {
                                //if success then show toast and finish
                                Toast.makeText(EditNoteActivity.this, R.string.update_success, Toast.LENGTH_SHORT).show();
                                finish();
                            } else {
                                Toast.makeText(EditNoteActivity.this, R.string.error_msg_common, Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(EditNoteActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
                        }
                    }
                };

                DatabaseRequestHandler.updateNote(mNote, mUpdateNoteCallback);
            }
        }
    }

    private void changeNoteTitle() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.enter_title);

        // Set up the input
        final EditText etNoteTitle = new EditText(this);
        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        etNoteTitle.setInputType(InputType.TYPE_CLASS_TEXT);

        //set hint text
        etNoteTitle.setHint(R.string.enter_title);

        //set previous text
        etNoteTitle.setText(mNote.title);

        //set maxLines 1
        etNoteTitle.setMaxLines(1);

        //set max length for edit text (50 characters)
        etNoteTitle.setFilters(new InputFilter[]{new InputFilter.LengthFilter(50)});


        //set view for alert dialog
        builder.setView(etNoteTitle);

        // Set up the buttons
        builder.setPositiveButton("OK", null);
        builder.setNegativeButton("Cancel", null);

        final AlertDialog alertDialog = builder.create();

        //set up button listeners
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Button btnOk = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);

                //set listener on ok button
                btnOk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String title = etNoteTitle.getText().toString().trim();

                        if (TextUtils.isEmpty(title)) {
                            //set title field empty
                            etNoteTitle.setText("");

                            //set "Field Required" error
                            etNoteTitle.setError(EditNoteActivity.this.getString(R.string.error_msg_field_required));

                            etNoteTitle.requestFocus();
                        } else {
                            mNote.title = title;
                        }

                        alertDialog.dismiss();
                    }
                });
            }
        });

        builder.setCancelable(true).show();
    }

    //return true if lock/unlock handled successfully
    private void lockUnlockNote() {
        //if note is locked then unlock it otherwise lock it
        if (mNote.isLocked) {
            mNote.isLocked = false;

            mLockMenuItem.setTitle(R.string.menu_lock);

        } else {
            //if shared preferences contains password then lock otherwise prompt user to set password
            if (PreferencesRequestHandler.isPasswordSet()) {
                mNote.isLocked = true;

                mLockMenuItem.setTitle(R.string.menu_unlock);
            } else {
                //initialize callback
                mPasswordSetCallback = new PreferencesRequestHandler.PasswordSetCallback() {
                    @Override
                    public void onPasswordSet(boolean success, int messageId) {
                        //unset the callback
                        mPasswordSetCallback = null;

                        if (success) {
                            mNote.isLocked = true;

                            mLockMenuItem.setTitle(R.string.menu_unlock);
                        }

                        Toast.makeText(EditNoteActivity.this, messageId, Toast.LENGTH_SHORT).show();
                    }
                };

                //prompt user to save password
                PreferencesRequestHandler.promptUserToSetPassword(new WeakReference<Context>(this),
                        new WeakReference<>(mPasswordSetCallback));
            }
        }
    }

    private void shareNote() {
        String shareText = mNote.title + "\n" + mNote.description;
        ShareCompat.IntentBuilder
                .from(this)
                .setChooserTitle(R.string.share_note)
                .setType("text/plain")
                .setText(shareText)
                .startChooser();
    }

    private void showDeleteNoteAlert() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.delete_note);

        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                deleteNote();
            }
        });

        builder.setNegativeButton("Cancel", null);

        builder.setCancelable(false).show();
    }

    private void deleteNote() {

        //show progress dialog
        if (mPDEditNotes != null) {
            mPDEditNotes.show();
        }

        mDeleteNoteCallback = new TaskFinished<Boolean>() {
            @Override
            public void onTaskFinished(Boolean success, String errorMessage) {
                //unset the callback
                mDeleteNoteCallback = null;

                //dismiss the dialog if not null (checking null to prevent not attached to window error)
                if (mPDEditNotes != null) {
                    mPDEditNotes.dismiss();
                }

                //if error message is not null then show error message
                if (TextUtils.isEmpty(errorMessage)) {
                    if (success != null && success) {
                        //if success then show toast and finish
                        Toast.makeText(EditNoteActivity.this, R.string.delete_success, Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        Toast.makeText(EditNoteActivity.this, R.string.error_msg_common, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(EditNoteActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
                }
            }
        };

        DatabaseRequestHandler.updateNotesDeleteStatus(mNote.noteId, !mNote.isDeleted, mDeleteNoteCallback);
    }
}
