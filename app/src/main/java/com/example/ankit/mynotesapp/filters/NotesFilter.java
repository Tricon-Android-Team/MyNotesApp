package com.example.ankit.mynotesapp.filters;

import android.content.Context;
import android.text.TextUtils;
import android.widget.Filter;

import com.example.ankit.mynotesapp.models.Note;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class NotesFilter extends Filter {

    private WeakReference<Context> mContextWeakReference;
    private WeakReference<List<Note>> mNotesWeakReference;

    public NotesFilter(Context context, List<Note> notes) {
        mContextWeakReference = new WeakReference<>(context);
        mNotesWeakReference = new WeakReference<>(notes);
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {

        Context context = mContextWeakReference.get();
        List<Note> notes = mNotesWeakReference.get();

        if (context != null && notes != null) {
            String filterString = constraint == null ? "" : constraint.toString();

            FilterResults filterResults = new FilterResults();

            if (TextUtils.isEmpty(filterString)) {
                filterResults.values = notes;
                filterResults.count = notes.size();
            } else {
                List<Note> filteredNotes = new ArrayList<>();

                for (int i = 0, length = notes.size(); i < length; i++) {
                    if (notes.get(i).title.contains(filterString)) {
                        filteredNotes.add(notes.get(i));
                    }
                }

                filterResults.values = filteredNotes;
                filterResults.count = filteredNotes.size();
            }

            return filterResults;
        }

        return null;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        Context context = mContextWeakReference.get();

        if (context != null) {
            if (results != null) {
                ((FilterResultListener) context).onFilter((List<Note>) results.values);
            } else {
                List<Note> notes = mNotesWeakReference.get();
                if (notes != null) {
                    ((FilterResultListener) context).onFilter(notes);
                }
            }
        }
    }

    public interface FilterResultListener {
        void onFilter(List<Note> notes);
    }
}
