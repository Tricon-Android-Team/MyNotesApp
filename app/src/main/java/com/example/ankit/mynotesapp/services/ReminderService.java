package com.example.ankit.mynotesapp.services;

import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.text.TextUtils;

import com.example.ankit.mynotesapp.R;
import com.example.ankit.mynotesapp.interfaces.TaskFinished;
import com.example.ankit.mynotesapp.models.Note;
import com.example.ankit.mynotesapp.requestDataModels.DatabaseRequestHandler;
import com.google.android.gms.gcm.GcmTaskService;
import com.google.android.gms.gcm.TaskParams;

import java.util.Random;

public class ReminderService extends GcmTaskService {

    private TaskFinished<Note> mGetNoteCallback;

    public static final String REMINDER_NOTIFICATION_TITLE = "Reminder";

    public static final String EXTRA_REMINDER_ID = "KEY_REMINDER_ID";

    @Override
    public void onInitializeTasks() {
        super.onInitializeTasks();
        //TODO: Reinitialize pending reminders
    }

    @Override
    public int onRunTask(TaskParams taskParams) {

        int reminderId = taskParams.getExtras().getInt(EXTRA_REMINDER_ID);

        mGetNoteCallback = new TaskFinished<Note>() {
            @Override
            public void onTaskFinished(Note note, String errorMessage) {

                //unset the callback
                mGetNoteCallback = null;

                if (TextUtils.isEmpty(errorMessage) && note != null) {
                    NotificationCompat.Builder builder = new NotificationCompat.Builder(ReminderService.this)
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setContentTitle(REMINDER_NOTIFICATION_TITLE)
                            .setContentText(note.title);

                    NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(ReminderService.this);
                    notificationManagerCompat.notify(new Random().nextInt(), builder.build());
                }
            }
        };

        DatabaseRequestHandler.fetchNotesByReminderId(mGetNoteCallback, reminderId);

        return 0;
    }

    @Override
    public void onDestroy() {
        mGetNoteCallback = null;
        super.onDestroy();
    }
}
