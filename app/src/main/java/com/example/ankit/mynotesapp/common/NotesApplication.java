package com.example.ankit.mynotesapp.common;

import android.app.Application;
import android.content.Context;

public class NotesApplication extends Application {

    public static Context APP_CONTEXT;

    @Override
    public void onCreate() {
        super.onCreate();

        APP_CONTEXT = this;
    }
}
