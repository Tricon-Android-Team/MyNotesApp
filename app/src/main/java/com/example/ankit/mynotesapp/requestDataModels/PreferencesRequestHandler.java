package com.example.ankit.mynotesapp.requestDataModels;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.example.ankit.mynotesapp.R;
import com.example.ankit.mynotesapp.common.NotesApplication;
import com.example.ankit.mynotesapp.common.Utils;

import java.lang.ref.WeakReference;

public final class PreferencesRequestHandler {

    private static final String SHARED_PREF_NAME = "NOTES_PREF";
    private static final String KEY_PASSWORD = "KEY_PASSWORD";

    //message id for no message sent in callbacks
    public static final int NO_MESSAGE_ID = -1;

    private static SharedPreferences mSharedPreferences = NotesApplication.APP_CONTEXT
            .getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);

    private PreferencesRequestHandler() {
    }

    public static boolean setPassword(String password) {
        String encryptedPassword = Utils.sha1(password);

        return mSharedPreferences.edit()
                .putString(KEY_PASSWORD, encryptedPassword)
                .commit();
    }

    public static boolean validatePassword(String password) {
        String encryptedPassword = Utils.sha1(password);
        String savedPassword = mSharedPreferences.getString(KEY_PASSWORD, "");

        return savedPassword.equals(encryptedPassword);
    }

    public static boolean isPasswordSet() {
        return mSharedPreferences.contains(KEY_PASSWORD);
    }

    public static void promptUserToSetPassword(WeakReference<Context> contextWeakReference,
                                               final WeakReference<PasswordSetCallback> callbackWeakReference) {
        Context context = contextWeakReference.get();
        if (context != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle(R.string.enter_password);

            // Set up the input
            final EditText etPassword = new EditText(context);
            // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
            etPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());

            //set hint text
            etPassword.setHint(R.string.enter_password);

            //set max length for edit text
            etPassword.setFilters(new InputFilter[]{new InputFilter.LengthFilter(6)});

            //set view for alert dialog
            builder.setView(etPassword);

            // Set up the buttons
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    PasswordSetCallback callback = callbackWeakReference.get();

                    if (callback != null) {
                        String password = etPassword.getText().toString().trim();

                        //if user didn't provide password then return result "failed" through call back else save password and return result success
                        if (TextUtils.isEmpty(password)) {
                            callback.onPasswordSet(false, R.string.error_msg_password_set_failed);
                        } else {
                            setPassword(password);
                            callback.onPasswordSet(true, R.string.password_set_successfully);
                        }
                        dialog.dismiss();
                    }
                }
            });

            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    PasswordSetCallback callback = callbackWeakReference.get();

                    if (callback != null) {
                        callback.onPasswordSet(false, R.string.error_msg_password_set_failed);
                    }

                    dialog.dismiss();
                }
            });

            builder.setCancelable(false).show();
        }
    }

    public static void promptUserToResetPassword(WeakReference<Context> contextWeakReference,
                                                 final WeakReference<PasswordSetCallback> callbackWeakReference) {
        final Context context = contextWeakReference.get();
        if (context != null) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle(R.string.reset_password);

            // Set up the input
            final EditText etOldPassword = new EditText(context);
            final EditText etNewPassword = new EditText(context);
            // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
            etOldPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
            etNewPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());

            //set hint text
            etOldPassword.setHint(R.string.enter_old_password);
            etNewPassword.setHint(R.string.enter_new_password);

            //set maxLines 1
            etOldPassword.setMaxLines(1);
            etNewPassword.setMaxLines(1);

            //set max length for edit text
            etOldPassword.setFilters(new InputFilter[]{new InputFilter.LengthFilter(6)});
            etNewPassword.setFilters(new InputFilter[]{new InputFilter.LengthFilter(6)});

            //add above edit text in a vertical linear layout
            LinearLayout linearLayout = new LinearLayout(context);
            linearLayout.setOrientation(LinearLayout.VERTICAL);
            linearLayout.addView(etOldPassword);
            linearLayout.addView(etNewPassword);

            //set view for alert dialog
            builder.setView(linearLayout);

            // Set up the buttons
            builder.setPositiveButton("OK", null);
            builder.setNegativeButton("Cancel", null);

            final AlertDialog alertDialog = builder.create();

            // Set up listener on buttons
            alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface dialog) {
                    Button btnOk = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
                    Button btnCancel = alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE);

                    //set listener on OK button
                    btnOk.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            PasswordSetCallback callback = callbackWeakReference.get();

                            if (callback != null) {

                                String oldPassword = etOldPassword.getText().toString().trim();
                                String newPassword = etNewPassword.getText().toString().trim();

                                //if user didn't provide password then return result "failed" through call back else save password and return result success
                                if (TextUtils.isEmpty(oldPassword)) {
                                    //set old password field empty
                                    etOldPassword.setText("");

                                    //set "Field Required" error
                                    etOldPassword.setError(context.getString(R.string.error_msg_field_required));

                                    etOldPassword.requestFocus();
                                } else if (TextUtils.isEmpty(newPassword)) {
                                    //set old password field empty
                                    etNewPassword.setText("");

                                    //set "Field Required" error
                                    etNewPassword.setError(context.getString(R.string.error_msg_field_required));

                                    etNewPassword.requestFocus();
                                } else {
                                    //if old password is correct then reset password else prompt user to enter correct old password
                                    if (validatePassword(oldPassword)) {
                                        setPassword(newPassword);
                                        callback.onPasswordSet(true, R.string.password_reset_successfully);
                                        alertDialog.dismiss();
                                    } else {
                                        //set old password field empty
                                        etOldPassword.setText("");

                                        //set wrong password as hint text
                                        etOldPassword.setHint(oldPassword);

                                        //set error in edit text
                                        etOldPassword.setError(context.getString(R.string.error_msg_wrong_password));

                                        etOldPassword.requestFocus();
                                    }
                                }
                            }
                        }
                    });

                    //set listener on cancel button
                    btnCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            PasswordSetCallback callback = callbackWeakReference.get();

                            if (callback != null) {
                                callback.onPasswordSet(false, R.string.error_msg_password_reset_failed);
                            }

                            alertDialog.dismiss();
                        }
                    });
                }
            });

            //show alert dialog
            alertDialog.show();
        }
    }

    public static void promptUserToEnterPasswordAndValidate(WeakReference<Context> contextWeakReference,
                                                            final WeakReference<PasswordValidationCallback> callbackWeakReference) {
        Context context = contextWeakReference.get();
        if (context != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle(R.string.enter_password);

            // Set up the input
            final EditText etPassword = new EditText(context);
            // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
            etPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());

            //set hint text
            etPassword.setHint(R.string.enter_password);

            //set max length for edit text
            etPassword.setFilters(new InputFilter[]{new InputFilter.LengthFilter(6)});

            //set view for alert dialog
            builder.setView(etPassword);

            // Set up the buttons
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    PasswordValidationCallback callback = callbackWeakReference.get();

                    if (callback != null) {
                        String password = etPassword.getText().toString().trim();

                        //if user didn't provide password then return result "failed" through call back else validate password and return result success
                        if (!TextUtils.isEmpty(password) && validatePassword(password)) {
                            callback.onPasswordValidation(true, NO_MESSAGE_ID);
                        } else {
                            callback.onPasswordValidation(false, R.string.error_msg_wrong_password);
                        }
                        dialog.dismiss();
                    }
                }
            });

            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    PasswordValidationCallback callback = callbackWeakReference.get();

                    if (callback != null) {
                        callback.onPasswordValidation(false, NO_MESSAGE_ID);
                    }

                    dialog.dismiss();
                }
            });

            builder.setCancelable(false).show();
        }
    }

    public interface PasswordSetCallback {
        void onPasswordSet(boolean success, int messageId);
    }

    public interface PasswordValidationCallback {
        void onPasswordValidation(boolean success, int messageId);
    }

}
