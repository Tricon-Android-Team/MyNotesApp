package com.example.ankit.mynotesapp.requestDataModels;

import android.os.AsyncTask;

import com.example.ankit.mynotesapp.R;
import com.example.ankit.mynotesapp.common.NotesApplication;
import com.example.ankit.mynotesapp.databases.NotesContract;
import com.example.ankit.mynotesapp.databases.NotesDatabaseHelper;
import com.example.ankit.mynotesapp.interfaces.TaskFinished;
import com.example.ankit.mynotesapp.models.Note;
import com.example.ankit.mynotesapp.models.Reminder;

import java.lang.ref.WeakReference;
import java.util.List;

public final class DatabaseRequestHandler {

    public static final int SORT_BY_CREATED_DATE = 0;
    public static final int SORT_BY_LAST_EDITED_DATE = 1;

    private DatabaseRequestHandler() {
    }

    /***********************************************
     * GET FUNCTIONS
     ***********************************/
    public static void fetchNotes(TaskFinished<List<Note>> callback, int sortBy) {
        //create async task to fetch notes from DB.
        new FetchNotesTask(callback, sortBy).execute();
    }

    static class FetchNotesTask extends AsyncTask<Void, Void, List<Note>> {

        private WeakReference<TaskFinished<List<Note>>> mTaskFinishedWeakReference;
        private String mOrderBy;

        public FetchNotesTask(TaskFinished<List<Note>> callback, int sortBy) {
            mTaskFinishedWeakReference = new WeakReference<>(callback);

            mOrderBy = sortBy == SORT_BY_LAST_EDITED_DATE ?
                    NotesContract.Note.COLUMN_LAST_EDITED_TIME : NotesContract.Note.COLUMN_CREATED_TIME;
        }

        @Override
        protected List<Note> doInBackground(Void... params) {
            return NotesDatabaseHelper.getInstance().getNotes(mOrderBy);
        }

        @Override
        protected void onPostExecute(List<Note> notes) {
            TaskFinished<List<Note>> callback = mTaskFinishedWeakReference.get();

            //if callback is not null then return result
            if (callback != null) {
                callback.onTaskFinished(notes, null);
            }
        }
    }

    public static void getReminder(TaskFinished<Reminder> callback, long reminderId) {
        //create async task to fetch notes from DB.
        new GetReminderTask(callback, reminderId).execute();
    }

    static class GetReminderTask extends AsyncTask<Void, Void, Reminder> {

        private WeakReference<TaskFinished<Reminder>> mTaskFinishedWeakReference;
        private long mReminderId;

        public GetReminderTask(TaskFinished<Reminder> callback, long reminderId) {
            mTaskFinishedWeakReference = new WeakReference<>(callback);
            mReminderId = reminderId;
        }

        @Override
        protected Reminder doInBackground(Void... params) {
            return NotesDatabaseHelper.getInstance().getReminder(mReminderId);
        }

        @Override
        protected void onPostExecute(Reminder reminder) {
            TaskFinished<Reminder> callback = mTaskFinishedWeakReference.get();

            //if callback is not null then return result
            if (callback != null) {
                callback.onTaskFinished(reminder, null);
            }
        }
    }

    public static void fetchNotesByReminderId(TaskFinished<Note> callback, int reminderId) {
        //create async task to fetch notes from DB.
        new FetchNotesByReminderIdTask(callback, reminderId).execute();
    }

    static class FetchNotesByReminderIdTask extends AsyncTask<Void, Void, Note> {

        private WeakReference<TaskFinished<Note>> mTaskFinishedWeakReference;
        private int mReminderId;

        public FetchNotesByReminderIdTask(TaskFinished<Note> callback, int reminderId) {
            mTaskFinishedWeakReference = new WeakReference<>(callback);

            mReminderId = reminderId;
        }

        @Override
        protected Note doInBackground(Void... params) {
            return NotesDatabaseHelper.getInstance().getNoteByReminderId(mReminderId);
        }

        @Override
        protected void onPostExecute(Note note) {
            TaskFinished<Note> callback = mTaskFinishedWeakReference.get();

            //if callback is not null then return result
            if (callback != null) {
                callback.onTaskFinished(note, null);
            }
        }
    }

    /*******************************************
     * Insert FUNCTIONS
     ***********************************/
    public static void insertNote(Note note, TaskFinished<Boolean> callback) {
        //create async task to insert note and insert in DB.
        //No need to cancel this as it should either save the note or fail.
        new InsertNoteTask(note, callback).execute();
    }

    static class InsertNoteTask extends AsyncTask<Void, Void, Boolean> {

        private Note mNote;
        private WeakReference<TaskFinished<Boolean>> mTaskFinishedWeakReference;

        public InsertNoteTask(Note note, TaskFinished<Boolean> callback) {
            mNote = note;
            mTaskFinishedWeakReference = new WeakReference<>(callback);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            return NotesDatabaseHelper.getInstance().insertNote(mNote);
        }

        @Override
        protected void onPostExecute(Boolean success) {
            TaskFinished<Boolean> callback = mTaskFinishedWeakReference.get();

            //if callback is not null then return result
            if (callback != null) {
                if (success) {
                    callback.onTaskFinished(true, null);
                } else {
                    callback.onTaskFinished(false, NotesApplication.APP_CONTEXT.getString(R.string.error_while_insert));
                }
            }
        }
    }

    public static void insertReminder(Reminder reminder, TaskFinished<Long> callback) {
        //create async task to insert reminder and insert in DB.
        new InsertReminderTask(reminder, callback).execute();
    }

    static class InsertReminderTask extends AsyncTask<Void, Void, Long> {

        private Reminder mReminder;
        private WeakReference<TaskFinished<Long>> mTaskFinishedWeakReference;

        public InsertReminderTask(Reminder reminder, TaskFinished<Long> callback) {
            mReminder = reminder;
            mTaskFinishedWeakReference = new WeakReference<>(callback);
        }

        @Override
        protected Long doInBackground(Void... params) {
            return NotesDatabaseHelper.getInstance().insertReminder(mReminder);
        }

        @Override
        protected void onPostExecute(Long reminderId) {
            TaskFinished<Long> callback = mTaskFinishedWeakReference.get();

            //if callback is not null then return result
            if (callback != null) {
                if (reminderId != null && reminderId != -1) {
                    callback.onTaskFinished(reminderId, null);
                } else {
                    callback.onTaskFinished(reminderId, NotesApplication.APP_CONTEXT.getString(R.string.error_while_insert));
                }
            }
        }
    }

    /********************************************
     * UPDATE FUNCTIONS
     ***********************************/
    public static void updateNotesLockedStatus(int noteId, boolean isLocked, TaskFinished<Boolean> callback) {
        //create async task to update note's locked status in DB.
        new UpdateNotesLockedStatusTask(noteId, isLocked, callback).execute();
    }

    static class UpdateNotesLockedStatusTask extends AsyncTask<Void, Void, Boolean> {

        private int mNoteId;
        private boolean mIsLocked;
        private WeakReference<TaskFinished<Boolean>> mTaskFinishedWeakReference;

        public UpdateNotesLockedStatusTask(int noteId, boolean isLocked, TaskFinished<Boolean> callback) {
            mNoteId = noteId;
            mIsLocked = isLocked;
            mTaskFinishedWeakReference = new WeakReference<>(callback);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            return NotesDatabaseHelper.getInstance().updateNotesLockedStatus(mNoteId, mIsLocked);
        }

        @Override
        protected void onPostExecute(Boolean success) {
            TaskFinished<Boolean> callback = mTaskFinishedWeakReference.get();

            //if callback is not null then return result
            if (callback != null) {
                if (success) {
                    callback.onTaskFinished(true, null);
                } else {
                    callback.onTaskFinished(false, NotesApplication.APP_CONTEXT.getString(R.string.error_while_update));
                }
            }
        }
    }

    public static void updateNotesTitle(int noteId, String title, TaskFinished<Boolean> callback) {
        //create async task to update note's title in DB.
        new UpdateNotesTitleTask(noteId, title, callback).execute();
    }

    static class UpdateNotesTitleTask extends AsyncTask<Void, Void, Boolean> {

        private int mNoteId;
        private String mTitle;
        private WeakReference<TaskFinished<Boolean>> mTaskFinishedWeakReference;

        public UpdateNotesTitleTask(int noteId, String title, TaskFinished<Boolean> callback) {
            mNoteId = noteId;
            mTitle = title;
            mTaskFinishedWeakReference = new WeakReference<>(callback);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            return NotesDatabaseHelper.getInstance().updateNotesTitle(mNoteId, mTitle);
        }

        @Override
        protected void onPostExecute(Boolean success) {
            TaskFinished<Boolean> callback = mTaskFinishedWeakReference.get();

            //if callback is not null then return result
            if (callback != null) {
                if (success) {
                    callback.onTaskFinished(true, null);
                } else {
                    callback.onTaskFinished(false, NotesApplication.APP_CONTEXT.getString(R.string.error_while_update));
                }
            }
        }
    }

    public static void updateNote(Note note, TaskFinished<Boolean> callback) {
        //create async task to update note in DB.
        new UpdateNoteTask(note, callback).execute();
    }

    static class UpdateNoteTask extends AsyncTask<Void, Void, Boolean> {

        private Note mNote;
        private WeakReference<TaskFinished<Boolean>> mTaskFinishedWeakReference;

        public UpdateNoteTask(Note note, TaskFinished<Boolean> callback) {
            mNote = note;
            mTaskFinishedWeakReference = new WeakReference<>(callback);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            return NotesDatabaseHelper.getInstance().updateNote(mNote);
        }

        @Override
        protected void onPostExecute(Boolean success) {
            TaskFinished<Boolean> callback = mTaskFinishedWeakReference.get();

            //if callback is not null then return result
            if (callback != null) {
                if (success) {
                    callback.onTaskFinished(true, null);
                } else {
                    callback.onTaskFinished(false, NotesApplication.APP_CONTEXT.getString(R.string.error_while_update));
                }
            }
        }
    }

    /********************************************
     * DELETE FUNCTIONS
     ***********************************/
    public static void updateNotesDeleteStatus(int noteId, boolean isDeleted, TaskFinished<Boolean> callback) {
        //create async task to delete note from DB.
        new UpdateNotesDeleteStatusTask(noteId, isDeleted, callback).execute();

    }

    static class UpdateNotesDeleteStatusTask extends AsyncTask<Void, Void, Boolean> {

        private int mNoteId;
        private boolean mIsDeleted;
        private WeakReference<TaskFinished<Boolean>> mTaskFinishedWeakReference;

        public UpdateNotesDeleteStatusTask(int noteId, boolean isDeleted, TaskFinished<Boolean> callback) {
            mNoteId = noteId;
            mIsDeleted = isDeleted;
            mTaskFinishedWeakReference = new WeakReference<>(callback);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            return NotesDatabaseHelper.getInstance().updateNotesDeleteStatus(mNoteId, mIsDeleted);
        }

        @Override
        protected void onPostExecute(Boolean success) {
            TaskFinished<Boolean> callback = mTaskFinishedWeakReference.get();

            //if callback is not null then return result
            if (callback != null) {
                if (success) {
                    callback.onTaskFinished(true, null);
                } else {
                    callback.onTaskFinished(false, NotesApplication.APP_CONTEXT.getString(R.string.error_while_update));
                }
            }
        }
    }

}
