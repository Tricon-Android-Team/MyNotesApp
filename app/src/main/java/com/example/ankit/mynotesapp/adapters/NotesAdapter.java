package com.example.ankit.mynotesapp.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.ankit.mynotesapp.R;
import com.example.ankit.mynotesapp.models.Note;

import java.lang.ref.WeakReference;
import java.util.List;

import viewholders.NotesHolder;

public class NotesAdapter extends RecyclerView.Adapter<NotesHolder> {

    private WeakReference<Context> mContextWeakReference;
    private List<Note> mNotes;

    public NotesAdapter(Context context, List<Note> notes, int sortBy) {
        this.mContextWeakReference = new WeakReference<>(context);
        this.mNotes = notes;
        NotesHolder.setSortBy(sortBy);
    }

    @Override
    public NotesHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        Context context = mContextWeakReference.get();

        if (context != null) {
            return new NotesHolder(LayoutInflater.from(context).inflate(R.layout.adapter_note_item, parent, false),
                    mContextWeakReference);
        } else {
            return null;
        }
    }

    @Override
    public void onBindViewHolder(NotesHolder holder, int position) {
        holder.bindData(mNotes.get(position));
    }

    @Override
    public int getItemCount() {
        return mNotes.size();
    }

    public void setSortBy(int sortBy) {
        NotesHolder.setSortBy(sortBy);
    }
}
