package com.example.ankit.mynotesapp.databases;

public final class NotesContract {

    private NotesContract() {
    }

    public static final int DB_VERSION = 1;

    public static final String DB_NAME = "notes.db";

    public static final class Note {

        public static final String TABLE_NAME = "notes";

        public static final String COLUMN_NOTE_ID = "noteId";

        public static final String COLUMN_TITLE = "title";

        public static final String COLUMN_DESCRIPTION = "description";

        public static final String COLUMN_CREATED_TIME = "createdTime";

        public static final String COLUMN_LAST_EDITED_TIME = "lastEditedTime";

        public static final String COLUMN_REMINDER_ID = "reminderId";

        public static final String COLUMN_IS_LOCKED = "isLocked";

        public static final String COLUMN_IS_DELETED = "isDeleted";

    }

    public static final class Reminder {

        public static final String TABLE_NAME = "reminders";

        public static final String COLUMN_REMINDER_ID = "reminderId";

        public static final String COLUMN_TIME = "time";

    }

}
