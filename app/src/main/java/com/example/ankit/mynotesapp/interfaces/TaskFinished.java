package com.example.ankit.mynotesapp.interfaces;

public interface TaskFinished<T> {
    void onTaskFinished(T data, String errorMessage);
}
