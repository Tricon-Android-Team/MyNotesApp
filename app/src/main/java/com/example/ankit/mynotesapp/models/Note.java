package com.example.ankit.mynotesapp.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Note implements Parcelable {

    public static final int NO_REMINDER = -1;

    //note's id
    public int noteId;

    //note's title
    public String title;

    //note's description (note's content)
    public String description;

    //note's create time
    public long createdTime;

    //note's edit time
    public long lastEditedTime;

    //reminder id
    public int reminderId = NO_REMINDER;          //foreign key

    //boolean to check if note is locked by password or not
    public boolean isLocked;

    //to check if note is deleted ot not
    public boolean isDeleted;

    public Note() {
    }

    public Note(int noteId, String title, String description,
                long createdTime, long lastEditedTime, int reminderId,
                boolean isLocked, boolean isDeleted) {
        this.noteId = noteId;
        this.title = title;
        this.description = description;
        this.createdTime = createdTime;
        this.lastEditedTime = lastEditedTime;
        this.reminderId = reminderId;
        this.isLocked = isLocked;
        this.isDeleted = isDeleted;
    }

    protected Note(Parcel in) {
        noteId = in.readInt();
        title = in.readString();
        description = in.readString();
        createdTime = in.readLong();
        lastEditedTime = in.readLong();
        reminderId = in.readInt();
        isLocked = in.readByte() != 0;
        isDeleted = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(noteId);
        dest.writeString(title);
        dest.writeString(description);
        dest.writeLong(createdTime);
        dest.writeLong(lastEditedTime);
        dest.writeInt(reminderId);
        dest.writeByte((byte) (isLocked ? 1 : 0));
        dest.writeByte((byte) (isDeleted ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Note> CREATOR = new Creator<Note>() {
        @Override
        public Note createFromParcel(Parcel in) {
            return new Note(in);
        }

        @Override
        public Note[] newArray(int size) {
            return new Note[size];
        }
    };
}
