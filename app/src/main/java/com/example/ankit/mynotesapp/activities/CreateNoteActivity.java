package com.example.ankit.mynotesapp.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.transition.Explode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.ankit.mynotesapp.R;
import com.example.ankit.mynotesapp.customs.LinedEditText;
import com.example.ankit.mynotesapp.interfaces.TaskFinished;
import com.example.ankit.mynotesapp.models.Note;
import com.example.ankit.mynotesapp.models.Reminder;
import com.example.ankit.mynotesapp.requestDataModels.DatabaseRequestHandler;
import com.example.ankit.mynotesapp.requestDataModels.PreferencesRequestHandler;
import com.example.ankit.mynotesapp.services.ReminderService;
import com.google.android.gms.gcm.GcmNetworkManager;
import com.google.android.gms.gcm.OneoffTask;
import com.google.android.gms.gcm.Task;

import java.lang.ref.WeakReference;

public class CreateNoteActivity extends AppCompatActivity {

    private LinedEditText mETDescription;

    private ProgressDialog mPDCreateNotes;

    //insert callback
    private TaskFinished<Boolean> mInsertNoteCallback;
    private TaskFinished<Reminder> mGetReminderCallback;
    private PreferencesRequestHandler.PasswordSetCallback mPasswordSetCallback;

    //variable to track if note is password locked or not
    private boolean mIsLocked;

    //if user presses back then try auto saving note. But if auto save fails then finish without saving
    private boolean mCanFinishWithoutSave;

    public static final int REQUEST_SET_REMINDER = 1000;

    private long mReminderId = Note.NO_REMINDER;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_note);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // set an exit transition
            getWindow().setExitTransition(new Explode());

            // set an enter transition
            getWindow().setEnterTransition(new Explode());
        }

        //set toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (toolbar != null) {
            toolbar.setNavigationIcon(R.drawable.ic_close_white);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

        //set views
        mETDescription = (LinedEditText) findViewById(R.id.et_description);

        //set progress dialog
        mPDCreateNotes = new ProgressDialog(this);
        mPDCreateNotes.setCancelable(false);
        mPDCreateNotes.setMessage("Please wait...");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_create_note, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_save:
                //set mCanFinishWithoutSave to false
                mCanFinishWithoutSave = false;

                //validate changes and save note
                validateChangesAndSave(true);
                break;

            case R.id.action_lock:
                //if note is locked then unlock it otherwise lock it
                if (mIsLocked) {
                    mIsLocked = false;
                    item.setIcon(R.drawable.ic_lock_open_white_24dp);
                } else {
                    //if shared preferences contains password then lock otherwise prompt user to set password
                    if (PreferencesRequestHandler.isPasswordSet()) {
                        mIsLocked = true;
                        item.setIcon(R.drawable.ic_lock_close_white_24dp);
                    } else {
                        //initialize callback
                        mPasswordSetCallback = new PreferencesRequestHandler.PasswordSetCallback() {
                            @Override
                            public void onPasswordSet(boolean success, int messageId) {
                                //unset the callback
                                mPasswordSetCallback = null;

                                if (success) {
                                    mIsLocked = true;
                                }

                                Toast.makeText(CreateNoteActivity.this, messageId, Toast.LENGTH_SHORT).show();
                            }
                        };

                        //prompt user to save password
                        PreferencesRequestHandler.promptUserToSetPassword(new WeakReference<Context>(this),
                                new WeakReference<>(mPasswordSetCallback));
                    }
                }
                break;

            case R.id.action_set_reminder:
                setReminder();
                break;

            case R.id.action_change_background:
                break;

            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQUEST_SET_REMINDER:
                if (resultCode == RESULT_OK) {
                    mReminderId = data.getLongExtra(SetReminderActivity.INTENT_EXTRA_REMINDER_ID, Note.NO_REMINDER);
                }

                break;

            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {

        //set mCanFinishWithoutSave to true
        mCanFinishWithoutSave = true;

        //validate changes and save note
        validateChangesAndSave(false);
    }

    @Override
    public void finish() {

        //dismiss dialog if not null and is shown and make it null
        if (mPDCreateNotes != null) {
            mPDCreateNotes.dismiss();
            mPDCreateNotes = null;
        }

        //unset callbacks if set
        mInsertNoteCallback = null;
        mGetReminderCallback = null;
        mPasswordSetCallback = null;

        setResult(RESULT_OK);
        super.finish();
    }

    //If user has typed some note and pressed back then don't ask title, auto set title as first word of the note and save
    //If user pressed save menu option then ask for title and save
    private void validateChangesAndSave(boolean askForTitle) {
        if (mETDescription != null) {
            String description = mETDescription.getText().toString().trim();

            if (TextUtils.isEmpty(description)) {
                finish();
            } else {
                if (askForTitle) {
                    showAddTitleDialog(description);
                } else {
                    String title = description.split(" ")[0];
                    saveNote(title, description);
                }
            }
        }
    }

    private void showAddTitleDialog(final String description) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.enter_title);

        // Set up the input
        final EditText etNoteTitle = new EditText(this);
        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        etNoteTitle.setInputType(InputType.TYPE_CLASS_TEXT);

        //set hint text
        etNoteTitle.setHint(R.string.enter_title);

        //set maxLines 1
        etNoteTitle.setMaxLines(1);

        //set max length for edit text (50 characters)
        etNoteTitle.setFilters(new InputFilter[]{new InputFilter.LengthFilter(50)});


        //set view for alert dialog
        builder.setView(etNoteTitle);

        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String title = etNoteTitle.getText().toString().trim();

                //if user didn't provide title then auto set title as first word of the note
                if (TextUtils.isEmpty(title)) {
                    title = description.split(" ")[0];
                }

                //save the note
                saveNote(title, description);

                dialog.dismiss();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //auto set title as first word of the note
                String title = description.split(" ")[0];
                saveNote(title, description);
                dialog.dismiss();
            }
        });

        builder.setCancelable(true).show();

    }

    //create note and insert in db
    private void saveNote(String title, String description) {

        //show progress dialog
        mPDCreateNotes.show();

        //create note
        final Note note = new Note();

        note.title = title;
        note.description = description;
        note.createdTime = System.currentTimeMillis();
        note.lastEditedTime = System.currentTimeMillis();
        note.reminderId = (int) mReminderId;
        note.isLocked = mIsLocked;
        note.isDeleted = false;

        //initialize callback
        mInsertNoteCallback = new TaskFinished<Boolean>() {
            @Override
            public void onTaskFinished(Boolean success, String errorMessage) {
                //unset the callback
                mInsertNoteCallback = null;

                //dismiss the dialog if not null (checking null to prevent not attached to window error)
                if (mPDCreateNotes != null) {
                    mPDCreateNotes.dismiss();
                }

                //if error message is not null then show error message else finish
                if (TextUtils.isEmpty(errorMessage)) {
                    if (success != null && success) {

                        //if success then show toast and finish
                        Toast.makeText(CreateNoteActivity.this, R.string.insert_success, Toast.LENGTH_SHORT).show();

                        //if reminder is set then start GcmTaskService
                        scheduleReminder();
                    } else {
                        Toast.makeText(CreateNoteActivity.this, R.string.error_msg_common, Toast.LENGTH_SHORT).show();

                        //if can finish without saving note then finish
                        if (mCanFinishWithoutSave) {
                            finish();
                        }
                    }
                } else {
                    Toast.makeText(CreateNoteActivity.this, errorMessage, Toast.LENGTH_SHORT).show();

                    //if can finish without saving note then finish
                    if (mCanFinishWithoutSave) {
                        finish();
                    }
                }
            }
        };

        //insert note in DB
        DatabaseRequestHandler.insertNote(note, mInsertNoteCallback);
    }

    private void setReminder() {
        startActivityForResult(new Intent(this, SetReminderActivity.class), REQUEST_SET_REMINDER);
    }

    private void scheduleReminder() {

        mGetReminderCallback = new TaskFinished<Reminder>() {
            @Override
            public void onTaskFinished(Reminder reminder, String errorMessage) {

                //unset the callback
                mGetReminderCallback = null;

                //dismiss the dialog if not null (checking null to prevent not attached to window error)
                if (mPDCreateNotes != null) {
                    mPDCreateNotes.dismiss();
                }

                if(TextUtils.isEmpty(errorMessage)) {

                    if (reminder != null) {

                        //schedule task
                        Bundle extras = new Bundle();
                        extras.putLong(ReminderService.EXTRA_REMINDER_ID, mReminderId);

                        long time = reminder.time;

                        long endTime = time - System.currentTimeMillis();
                        long startTime = endTime - 300L;    //in 5 min window

                        if(endTime < 0) {
                            endTime = 0;
                        }

                        if(startTime < 0) {
                            startTime = 0;
                        }

                        OneoffTask task = new OneoffTask.Builder()
                                .setService(ReminderService.class)
                                .setExtras(extras)
                                .setTag(ReminderService.REMINDER_NOTIFICATION_TITLE)
                                .setExecutionWindow(startTime, endTime)
                                .setPersisted(true)
                                .build();

                        GcmNetworkManager.getInstance(CreateNoteActivity.this).schedule(task);
                    } else {
                        Toast.makeText(CreateNoteActivity.this, R.string.error_msg_common, Toast.LENGTH_SHORT).show();

                        //if can finish without saving note then finish
                        if (mCanFinishWithoutSave) {
                            finish();
                        }
                    }
                } else {
                    Toast.makeText(CreateNoteActivity.this, errorMessage, Toast.LENGTH_SHORT).show();

                    //if can finish without saving note then finish
                    if (mCanFinishWithoutSave) {
                        finish();
                    }
                }

                finish();
            }
        };

        DatabaseRequestHandler.getReminder(mGetReminderCallback, mReminderId);


    }
}
