package com.example.ankit.mynotesapp.databases;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;

import com.example.ankit.mynotesapp.common.NotesApplication;
import com.example.ankit.mynotesapp.models.Note;
import com.example.ankit.mynotesapp.models.Reminder;

import java.util.ArrayList;
import java.util.List;

public class NotesDatabaseHelper extends SQLiteOpenHelper {

    private static NotesDatabaseHelper mNotesDatabaseHelper;

    private NotesDatabaseHelper() {
        super(NotesApplication.APP_CONTEXT, NotesContract.DB_NAME, null, NotesContract.DB_VERSION);
    }

    public static NotesDatabaseHelper getInstance() {
        if (mNotesDatabaseHelper == null) {
            mNotesDatabaseHelper = new NotesDatabaseHelper();
        }

        return mNotesDatabaseHelper;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_NOTES_TABLE = "CREATE TABLE " + NotesContract.Note.TABLE_NAME + " (" +
                NotesContract.Note.COLUMN_NOTE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                NotesContract.Note.COLUMN_TITLE + " TEXT, " +
                NotesContract.Note.COLUMN_DESCRIPTION + " TEXT, " +
                NotesContract.Note.COLUMN_CREATED_TIME + " BIGINT, " +
                NotesContract.Note.COLUMN_LAST_EDITED_TIME + " BIGINT, " +
                NotesContract.Note.COLUMN_REMINDER_ID + " INTEGER, " +
                NotesContract.Note.COLUMN_IS_LOCKED + " TINYINT, " +
                NotesContract.Note.COLUMN_IS_DELETED + " TINYINT, " +
                "FOREIGN KEY (" + NotesContract.Note.COLUMN_REMINDER_ID + ") REFERENCES " +
                NotesContract.Reminder.TABLE_NAME + " (" + NotesContract.Reminder.COLUMN_REMINDER_ID + ")" + ")";

        String CREATE_NOTES_REMINDER = "CREATE TABLE " + NotesContract.Reminder.TABLE_NAME + " (" +
                NotesContract.Reminder.COLUMN_REMINDER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                NotesContract.Reminder.COLUMN_TIME + " BIGINT" + ")";

        db.execSQL(CREATE_NOTES_TABLE);
        db.execSQL(CREATE_NOTES_REMINDER);
    }

    @Override
    public void onConfigure(SQLiteDatabase db) {
        db.setForeignKeyConstraintsEnabled(true);
        super.onConfigure(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    /***************************************************
     * INSERT
     ***************************************************/
    public boolean insertNote(Note note) {
        String INSERT_INTO_NOTES = "INSERT INTO " + NotesContract.Note.TABLE_NAME + " (" +
                NotesContract.Note.COLUMN_TITLE + "," +
                NotesContract.Note.COLUMN_DESCRIPTION + "," +
                NotesContract.Note.COLUMN_CREATED_TIME + "," +
                NotesContract.Note.COLUMN_LAST_EDITED_TIME + "," +
                NotesContract.Note.COLUMN_REMINDER_ID + "," +
                NotesContract.Note.COLUMN_IS_LOCKED + "," +
                NotesContract.Note.COLUMN_IS_DELETED + ") VALUES(?,?,?,?,?,?,?)";

        SQLiteDatabase database = getWritableDatabase();
        SQLiteStatement statement = database.compileStatement(INSERT_INTO_NOTES);

        statement.bindString(1, note.title);
        statement.bindString(2, note.description);
        statement.bindLong(3, note.createdTime);
        statement.bindLong(4, note.lastEditedTime);
        if (Note.NO_REMINDER == note.reminderId) {
            statement.bindNull(5);
        } else {
            statement.bindLong(5, note.reminderId);
        }
        statement.bindLong(6, note.isLocked ? 1 : 0);
        statement.bindLong(7, note.isDeleted ? 1 : 0);

        long result = statement.executeInsert();

        statement.close();
        database.close();

        return result != -1;
    }

    public long insertReminder(Reminder reminder) {
        String INSERT_INTO_REMINDER = "INSERT INTO " + NotesContract.Reminder.TABLE_NAME + " (" +
                NotesContract.Reminder.COLUMN_TIME + ") VALUES(?)";

        SQLiteDatabase database = getWritableDatabase();
        SQLiteStatement statement = database.compileStatement(INSERT_INTO_REMINDER);

        statement.bindLong(1, reminder.time);

        long result = statement.executeInsert();

        statement.close();
        database.close();

        return result;
    }

    /***************************************************
     * SELECT
     ***************************************************/
    public List<Note> getNotes(String orderBy) {

        SQLiteDatabase database = getReadableDatabase();

        //select all notes for which isDeleted is false and order by lastEditedTime;
        String selectNotes = "SELECT * FROM " + NotesContract.Note.TABLE_NAME +
                " WHERE " + NotesContract.Note.COLUMN_IS_DELETED + " = ? ORDER BY " + orderBy + " DESC";

        Cursor cursor = database.rawQuery(selectNotes, new String[]{"0"});

        if (cursor != null) {
            List<Note> notes = new ArrayList<>(cursor.getCount());

            final int COLUMN_NOTE_ID = cursor.getColumnIndex(NotesContract.Note.COLUMN_NOTE_ID);
            final int COLUMN_TITLE = cursor.getColumnIndex(NotesContract.Note.COLUMN_TITLE);
            final int COLUMN_DESCRIPTION = cursor.getColumnIndex(NotesContract.Note.COLUMN_DESCRIPTION);
            final int COLUMN_CREATED_TIME = cursor.getColumnIndex(NotesContract.Note.COLUMN_CREATED_TIME);
            final int COLUMN_LAST_EDITED_TIME = cursor.getColumnIndex(NotesContract.Note.COLUMN_LAST_EDITED_TIME);
            final int COLUMN_REMINDER_ID = cursor.getColumnIndex(NotesContract.Note.COLUMN_REMINDER_ID);
            final int COLUMN_IS_LOCKED = cursor.getColumnIndex(NotesContract.Note.COLUMN_IS_LOCKED);
            final int COLUMN_IS_DELETED = cursor.getColumnIndex(NotesContract.Note.COLUMN_IS_DELETED);

            while (cursor.moveToNext()) {

                int reminderId = Note.NO_REMINDER;
                try {
                    reminderId = (int) cursor.getLong(COLUMN_REMINDER_ID);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                notes.add(new Note(cursor.getInt(COLUMN_NOTE_ID),
                        cursor.getString(COLUMN_TITLE),
                        cursor.getString(COLUMN_DESCRIPTION),
                        cursor.getLong(COLUMN_CREATED_TIME),
                        cursor.getLong(COLUMN_LAST_EDITED_TIME),
                        reminderId,
                        cursor.getShort(COLUMN_IS_LOCKED) == 1,
                        cursor.getShort(COLUMN_IS_DELETED) == 1));
            }
            cursor.close();
            return notes;
        } else {
            return new ArrayList<>(0);
        }
    }

    public Note getNoteByReminderId(int reminderId) {

        SQLiteDatabase database = getReadableDatabase();

        //select all notes for which isDeleted is false and order by lastEditedTime;
        String selectNote = "SELECT * FROM " + NotesContract.Note.TABLE_NAME +
                " WHERE " + NotesContract.Note.COLUMN_IS_DELETED + " = ? AND " +
                NotesContract.Note.COLUMN_REMINDER_ID + " = ?";

        Cursor cursor = database.rawQuery(selectNote, new String[]{"0", String.valueOf(reminderId)});

        if (cursor != null) {

            final int COLUMN_NOTE_ID = cursor.getColumnIndex(NotesContract.Note.COLUMN_NOTE_ID);
            final int COLUMN_TITLE = cursor.getColumnIndex(NotesContract.Note.COLUMN_TITLE);
            final int COLUMN_DESCRIPTION = cursor.getColumnIndex(NotesContract.Note.COLUMN_DESCRIPTION);
            final int COLUMN_CREATED_TIME = cursor.getColumnIndex(NotesContract.Note.COLUMN_CREATED_TIME);
            final int COLUMN_LAST_EDITED_TIME = cursor.getColumnIndex(NotesContract.Note.COLUMN_LAST_EDITED_TIME);
            final int COLUMN_IS_LOCKED = cursor.getColumnIndex(NotesContract.Note.COLUMN_IS_LOCKED);
            final int COLUMN_IS_DELETED = cursor.getColumnIndex(NotesContract.Note.COLUMN_IS_DELETED);


            cursor.moveToFirst();
            Note note = new Note(cursor.getInt(COLUMN_NOTE_ID),
                    cursor.getString(COLUMN_TITLE),
                    cursor.getString(COLUMN_DESCRIPTION),
                    cursor.getLong(COLUMN_CREATED_TIME),
                    cursor.getLong(COLUMN_LAST_EDITED_TIME),
                    reminderId,
                    cursor.getShort(COLUMN_IS_LOCKED) == 1,
                    cursor.getShort(COLUMN_IS_DELETED) == 1);

            cursor.close();
            return note;
        }

        return null;
    }

    //return list of reminder for time greater than equal to given time
    public List<Reminder> getReminders(long time) {

        SQLiteDatabase database = getReadableDatabase();

        //select all notes for which isDeleted is false and order by lastEditedTime;
        String selectNotes = "SELECT * FROM " + NotesContract.Reminder.TABLE_NAME +
                " WHERE " + NotesContract.Reminder.COLUMN_TIME + " >= ? ORDER BY " +
                NotesContract.Reminder.COLUMN_TIME + " ASC";

        Cursor cursor = database.rawQuery(selectNotes, new String[]{String.valueOf(time)});

        if (cursor != null) {
            List<Reminder> reminders = new ArrayList<>(cursor.getCount());

            final int COLUMN_REMINDER_ID = cursor.getColumnIndex(NotesContract.Reminder.COLUMN_REMINDER_ID);
            final int COLUMN_TIME = cursor.getColumnIndex(NotesContract.Reminder.COLUMN_TIME);

            while (cursor.moveToNext()) {

                reminders.add(new Reminder(cursor.getInt(COLUMN_REMINDER_ID),
                        cursor.getLong(COLUMN_TIME)));
            }
            cursor.close();
            return reminders;
        } else {
            return new ArrayList<>(0);
        }
    }

    //return reminder for reminderId
    public Reminder getReminder(long reminderId) {

        SQLiteDatabase database = getReadableDatabase();

        //select all notes for which isDeleted is false and order by lastEditedTime;
        String selectNotes = "SELECT * FROM " + NotesContract.Reminder.TABLE_NAME +
                " WHERE " + NotesContract.Reminder.COLUMN_REMINDER_ID + " = ?";

        Cursor cursor = database.rawQuery(selectNotes, new String[]{String.valueOf(reminderId)});

        if (cursor != null && cursor.getCount() == 1) {
            final int COLUMN_REMINDER_ID = cursor.getColumnIndex(NotesContract.Reminder.COLUMN_REMINDER_ID);
            final int COLUMN_TIME = cursor.getColumnIndex(NotesContract.Reminder.COLUMN_TIME);

            cursor.moveToFirst();

            Reminder reminder = new Reminder(cursor.getInt(COLUMN_REMINDER_ID),
                    cursor.getLong(COLUMN_TIME));

            cursor.close();
            return reminder;
        }

        return null;
    }

    /***************************************************
     * UPDATE
     ***************************************************/
    public boolean updateNotesTitle(int noteId, String newTitle) {

        String whereClause = NotesContract.Note.COLUMN_NOTE_ID + " = ?";

        ContentValues values = new ContentValues();
        values.put(NotesContract.Note.COLUMN_TITLE, newTitle);

        SQLiteDatabase database = getWritableDatabase();
        boolean success = database.update(NotesContract.Note.TABLE_NAME,
                values,
                whereClause,
                new String[]{String.valueOf(noteId)}) == 1;

        database.close();

        return success;
    }

    public boolean updateNotesDescription(int noteId, String description) {

        String whereClause = NotesContract.Note.COLUMN_NOTE_ID + " = ?";

        ContentValues values = new ContentValues();
        values.put(NotesContract.Note.COLUMN_DESCRIPTION, description);

        SQLiteDatabase database = getWritableDatabase();
        boolean success = database.update(NotesContract.Note.TABLE_NAME,
                values,
                whereClause,
                new String[]{String.valueOf(noteId)}) == 1;

        database.close();

        return success;
    }

    public boolean updateNotesLastEditedTime(int noteId, long lastEditedTime) {

        String whereClause = NotesContract.Note.COLUMN_NOTE_ID + " = ?";

        ContentValues values = new ContentValues();
        values.put(NotesContract.Note.COLUMN_LAST_EDITED_TIME, lastEditedTime);

        SQLiteDatabase database = getWritableDatabase();
        boolean success = database.update(NotesContract.Note.TABLE_NAME,
                values,
                whereClause,
                new String[]{String.valueOf(noteId)}) == 1;

        database.close();

        return success;
    }

    public boolean updateNotesDeleteStatus(int noteId, boolean isDeleted) {

        String whereClause = NotesContract.Note.COLUMN_NOTE_ID + " = ?";

        ContentValues values = new ContentValues();
        values.put(NotesContract.Note.COLUMN_IS_DELETED, isDeleted ? 1 : 0);

        SQLiteDatabase database = getWritableDatabase();
        boolean success = database.update(NotesContract.Note.TABLE_NAME,
                values,
                whereClause,
                new String[]{String.valueOf(noteId)}) == 1;

        database.close();

        return success;
    }

    public boolean updateNotesLockedStatus(int noteId, boolean isLocked) {

        String whereClause = NotesContract.Note.COLUMN_NOTE_ID + " = ?";

        ContentValues values = new ContentValues();
        values.put(NotesContract.Note.COLUMN_IS_LOCKED, isLocked ? 1 : 0);

        SQLiteDatabase database = getWritableDatabase();
        boolean success = database.update(NotesContract.Note.TABLE_NAME,
                values,
                whereClause,
                new String[]{String.valueOf(noteId)}) == 1;

        database.close();

        return success;
    }

    public boolean updateNote(Note note) {

        String whereClause = NotesContract.Note.COLUMN_NOTE_ID + " = ?";

        ContentValues values = new ContentValues();
        values.put(NotesContract.Note.COLUMN_TITLE, note.title);
        values.put(NotesContract.Note.COLUMN_DESCRIPTION, note.description);
        values.put(NotesContract.Note.COLUMN_CREATED_TIME, note.createdTime);
        values.put(NotesContract.Note.COLUMN_LAST_EDITED_TIME, note.lastEditedTime);
        values.put(NotesContract.Note.COLUMN_IS_LOCKED, note.isLocked ? 1 : 0);
        values.put(NotesContract.Note.COLUMN_IS_DELETED, note.isDeleted ? 1 : 0);

        SQLiteDatabase database = getWritableDatabase();
        boolean success = database.update(NotesContract.Note.TABLE_NAME,
                values,
                whereClause,
                new String[]{String.valueOf(note.noteId)}) == 1;

        database.close();

        return success;
    }

    public boolean updateReminder(Reminder reminder) {

        String whereClause = NotesContract.Reminder.COLUMN_REMINDER_ID + " = ?";

        ContentValues values = new ContentValues();
        values.put(NotesContract.Reminder.COLUMN_TIME, reminder.time);

        SQLiteDatabase database = getWritableDatabase();
        boolean success = database.update(NotesContract.Reminder.TABLE_NAME,
                values,
                whereClause,
                new String[]{String.valueOf(reminder.reminderId)}) == 1;

        database.close();

        return success;
    }

    /***************************************************
     * DELETE
     ***************************************************/
    public boolean deleteNotePermanently(int noteId) {

        String whereClause = NotesContract.Note.COLUMN_NOTE_ID + " = ?";
        SQLiteDatabase database = getWritableDatabase();
        boolean success = database.delete(NotesContract.Note.TABLE_NAME,
                whereClause,
                new String[]{String.valueOf(noteId)}) == 1;

        database.close();

        return success;
    }

    public boolean deleteReminder(int reminderId) {

        String whereClause = NotesContract.Reminder.COLUMN_REMINDER_ID + " = ?";
        SQLiteDatabase database = getWritableDatabase();
        boolean success = database.delete(NotesContract.Reminder.TABLE_NAME,
                whereClause,
                new String[]{String.valueOf(reminderId)}) == 1;

        database.close();

        return success;
    }

}
