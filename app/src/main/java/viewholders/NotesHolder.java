package viewholders;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ankit.mynotesapp.R;
import com.example.ankit.mynotesapp.activities.MyNotesActivity;
import com.example.ankit.mynotesapp.common.Utils;
import com.example.ankit.mynotesapp.models.Note;
import com.example.ankit.mynotesapp.requestDataModels.DatabaseRequestHandler;

import java.lang.ref.WeakReference;

public class NotesHolder extends RecyclerView.ViewHolder
        implements View.OnClickListener, View.OnCreateContextMenuListener {

    private WeakReference<Context> mContextWeakReference;

    private CardView cvNote;
    private TextView tvTime;
    private ImageView ivLockedStatus;
    private TextView tvTitle;
    private TextView tvDescription;
    private View separatorView;

    private static int mSortBy = DatabaseRequestHandler.SORT_BY_LAST_EDITED_DATE;

    public NotesHolder(View itemView, WeakReference<Context> contextWeakReference) {
        super(itemView);

        //set up views
        cvNote = (CardView) itemView.findViewById(R.id.cv_note);
        tvTime = (TextView) itemView.findViewById(R.id.tv_time);
        ivLockedStatus = (ImageView) itemView.findViewById(R.id.iv_locked_status);
        tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
        tvDescription = (TextView) itemView.findViewById(R.id.tv_description);
        separatorView = itemView.findViewById(R.id.separator_view);

        //store context weak reference
        mContextWeakReference = contextWeakReference;

        //set listeners
        cvNote.setOnClickListener(this);
        ivLockedStatus.setOnClickListener(this);

        //set context menu on note card
        cvNote.setOnCreateContextMenuListener(this);
    }

    public static void setSortBy(int sortBy) {
        mSortBy = sortBy;
    }

    public void bindData(Note note) {

        if (mSortBy == DatabaseRequestHandler.SORT_BY_CREATED_DATE) {
            tvTime.setText(Utils.getDate(note.createdTime));
        } else {
            tvTime.setText(Utils.getDate(note.lastEditedTime));
        }

        if (note.isLocked) {
            ivLockedStatus.setImageResource(R.drawable.ic_lock_close_black_24dp);
            tvTitle.setVisibility(View.GONE);
            tvDescription.setVisibility(View.GONE);
            separatorView.setVisibility(View.GONE);
        } else {
            ivLockedStatus.setImageResource(R.drawable.ic_lock_open_black_24dp);
            tvTitle.setVisibility(View.VISIBLE);
            tvDescription.setVisibility(View.VISIBLE);
            separatorView.setVisibility(View.VISIBLE);
            tvTitle.setText(note.title);
            tvDescription.setText(note.description);
        }
    }

    @Override
    public void onClick(View v) {

        Context context = mContextWeakReference.get();

        if (context != null) {
            switch (v.getId()) {
                case R.id.cv_note:
                    ((MyNotesActivity) context).startEditNoteActivity(getAdapterPosition(), v);
                    break;

                case R.id.iv_locked_status:
                    ((MyNotesActivity) context).changeLockedStatus(getAdapterPosition());
                    break;

                default:
                    break;
            }
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        Context context = mContextWeakReference.get();

        if (context != null) {
            ((MyNotesActivity) context).onCreateContextMenu(menu, getAdapterPosition());
        }
    }
}
